package com.optimusEcom.pages;

public enum ProductSize {
    XS, S, M, L, XL
}